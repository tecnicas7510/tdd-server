'use strict';

const express = require('express');
const { files } = require('../controllers/index');
const authenticateUser = require('../middleware/authenticate-user');

const router = express.Router();

router.post('/', authenticateUser, files.upload);
router.get('/', authenticateUser, files.getFilesAndDirectories);
router.get('/tree', authenticateUser, files.getTree);
router.get('/file/', authenticateUser, files.getFile);
router.delete('/file/', authenticateUser, files.deleteFile);
router.delete('/directory/', authenticateUser, files.deleteDirectory);

module.exports = router;
