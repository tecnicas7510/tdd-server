'use strict';

const userSchema = require('./user');
const fileSchema = require('./file');
const shareRequestSchema = require('./share-request');

module.exports = {
	userSchema,
	fileSchema,
	shareRequestSchema
};
