'use strict';

const users = require('./users');
const files = require('./files');

module.exports = {
	users,
	files
};
